#!/usr/bin/python
# This code is is dedicated to the public domain under the CC0 public domain
# dedication; see doc/cc0.txt for full information, or
#       <http://creativecommons.org/publicdomain/zero/1.0/>
# if you did not receive a copy.

"""Helper functinos for python2/python3 compatibility."""

__all__ = [ 'b', 'u' ]
import codecs
import sys

if sys.version_info < (3,):
    def b(x):
        return x
    def u(x):
        return x
    def iteritems(d):
        return d.iteritems()
    READ_TEXT = "r"
    WRITE_TEXT = "w"
    range_ = xrange
else:
    def b(x):
        return codecs.latin_1_encode(x)[0]
    def u(x):
        return str(x, "latin-1")
    def iteritems(d):
        return iter(d.items())
    READ_TEXT = "rt"
    WRITE_TEXT = "wt"
    range_ = range
