#!/usr/bin/python
# This code is is dedicated to the public domain under the CC0 public domain
# dedication; see doc/cc0.txt for full information, or
#       <http://creativecommons.org/publicdomain/zero/1.0/>
# if you did not receive a copy.

"""Code for moving functions from one module to another.  We use these to
   rewrite the callgraph based on function movement annotations before we
   apply the callgraph rules.
"""

class CallgraphRewriteRule(object):
    """A single rewrite rule."""
    def apply(self, cg):
        raise NotImplemented()

class CallgraphRewriter(CallgraphRewriteRule):
    """Compound rule: contains other rules and executes them in priority
       order (from lowest to highest)"""
    def __init__(self):
        self._rules = [ ]

    def addRule(self, rule, priority=0):
        self._rules.append((priority, rule))

    def apply(self, cg):
        cg.rebuildLocationMap()
        self._rules.sort()
        for _, r in self.rules:
            r.apply(cg)
        cg.rebuildLocationMap()

class MoveFunction(CallgraphRewriteRule):
    """Simple rule: moves a function from one module to another."""
    def __init__(self, func, destMod, srcMod=None):
        self._func = func
        self._destMod = destMod
        self._srcMod = srcMod

    def apply(self, cg):
        dest = cg.getOrCreateModule(self._destMod)

        if self._srcMod is not None:
            src = cg.getModule(self._srcMod)
        else:
            src = cg.getModuleDefining(self._func)

        src.moveFuncToModule(self._func, dest)


