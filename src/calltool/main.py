#!/usr/bin/python

# This code is is dedicated to the public domain under the CC0 public domain
# dedication; see doc/cc0.txt for full information, or
#       <http://creativecommons.org/publicdomain/zero/1.0/>
# if you did not receive a copy.

"""Main entry point.

   Right now this is just hardcoded to execute the codepaths here on Tor.
"""
# XXX write a real commandline.

from logging import warn, info
import os
import pprint
import sys



import calltool.CFile
import calltool.Callgraph
import calltool.BuildCallgraph
import calltool.CallgraphRules
import calltool.CallgraphRewrite
import calltool.CallgraphTools

def is_bogon(fname0):
    """
       Return True if we shouldn't actually analyze the object
       file called fname0.
    """
    fname = os.path.basename(fname0)
    if fname.startswith("src_test_test_slow-tinytest"):
        return True # kludge -- why is this module built twice?
    if fname in ("pwbox.o", "trunnel.o"):
        return True # kludge-- why is this module built twice?
    if fname.startswith("src_") and "_testing_" in fname:
        return True # We don't care about the callgraph of modules as
                    # compiled with testing support.
    elif "/src/test/" in fname0:
        return True # We don't care about the callgraph of the unit tests,
    elif "/src/tools/" in fname0:
        return True # We aren't looking at the tools yet.
    else:
        return False

def getfiles(path):
    """Generator: yield every file under path.  Much like 'find path -type f'
    """
    for dp,_,fns in os.walk(path):
        for fn in fns:
            yield os.path.join(dp, fn)

def objfiles(path):
    """Generator: Yield every object file under path."""
    for fn in getfiles(path):
        if fn.endswith(".o"):
            yield fn

def cfiles(path):
    """Generator: Yield every C file under path."""
    for fn in getfiles(path):
        if fn.endswith(".c") or fn.endswith(".h"):
            yield fn

def make_callgraph(binaries, src_path, obj_path):
    # 1. Construct the callgraph.

    cg = calltool.Callgraph.Callgraph()

    # Add the .o files

    for obj in objfiles(obj_path):
        if is_bogon(obj):
            info("Skipping %s", obj)
            continue
        info("Looking at %s", obj)
        mod = calltool.BuildCallgraph.from_objdump_and_nm_cached(obj)
        cg.addModule(mod)

    # Add the .so files
    for binary in binaries:
        for mod in calltool.BuildCallgraph.from_shared_libraries(binary):
            info("Adding %s from shared object", mod.getName())
            cg.addModule(mod)

    # 2. Build the callgraph rewriter and analyze the C files.
    rw = calltool.CallgraphRewrite.CallgraphRewriter()

    # Look at all the C files.
    cnames = set()
    for fn in cfiles(src_path):
        if is_bogon(fn):
            info("Skipping %s", fn)
            continue

        info("Looking at %s", fn)
        cfile = calltool.CFile.chopFile(fn)
        modname = calltool.BuildCallgraph.pretty_name(fn)
        cfile.setModuleName(modname)
        cnames.add(modname)
        # Add the moves from each file to the rewriter.
        cfile.addToCGRewriter(rw)
        try:
            m = cg.getModule(modname)
            for g in m.getGroups():
                m.addToGroup(g)
        except KeyError:
            if fn.endswith(".c") and "/ext" not in fn:
                warn("No object found for C module %s for %s.",modname,fn)

    # Make sure that we actually have a c module for each object file.
    # XXXX this includes .h files and shouldn't.
    for mn in cg._modulesByName:
        if mn in cnames:
            continue
        if not cg.getModule(mn).isExternal() and "/ext/" not in cg.getModule(mn).getFname():
            warn("Where is the C file for %s?",mn)

    return cg

def check_rules(callgraph):

    # Now, apply the rules about what is allowed to apply to what.
    rb = calltool.CallgraphRules.RuleBuilder()

    rb.succeedByDefault()

    # Find all the violations and display them.
    for violation in rb.getRules().violations(callgraph):
        print(violation)

def do_module_graph(cg):
    _, simple = cg.buildModuleCalls()
    pprint.pprint(simple)

def do_module_invgraph(cg):
    _, simple = cg.buildModuleCalls()
    simple = calltool.CallgraphTools.invert_graph(simple)
    pprint.pprint(simple)

def do_module_scc(cg):
    _, simple = cg.buildModuleCalls()

    all_sccs = calltool.CallgraphTools.strongly_connected_components(simple)

    all_sccs = sorted(
        (len(scc), scc) for scc in all_sccs
    )
    pprint.pprint(all_sccs)


def do_module_scc_weaklinks(cg):
    _, simple = cg.buildModuleCalls()

    a, b = calltool.CallgraphTools.connection_bottlenecks(simple)
    reductions = a + b
    reductions.sort()
    pprint.pprint(reductions)

def do_fn_graph(cg):
    calls = cg.getFunctionCalls()
    pprint.pprint(calls)

def do_fn_invgraph(cg):
    calls = calltool.CallgraphTools.invert_graph(cg.getFunctionCalls())
    pprint.pprint(calls)

def do_fn_scc(cg):
    calls = cg.getFunctionCalls()

    all_sccs = calltool.CallgraphTools.strongly_connected_components(calls)
    all_sccs = sorted(
        (len(scc), scc) for scc in all_sccs
    )
    pprint.pprint(all_sccs)


def do_fn_scc_weaklinks(cg):
    calls = cg.getFunctionCalls()

    a, b = calltool.CallgraphTools.connection_bottlenecks(calls)
    reductions = a + b
    reductions.sort()
    pprint.pprint(reductions)


def main(argv):

    if len(argv) <= 1:
        command = 'module_scc'
    else:
        command = argv[1]

    funcs = {
        'module_graph' : do_module_graph,
        'module_invgraph' : do_module_invgraph,
        'module_scc' : do_module_scc,
        'module_scc_weaklinks' : do_module_scc_weaklinks,
        'fn_graph' : do_fn_graph,
        'fn_invgraph' : do_fn_invgraph,
        'fn_scc' : do_fn_scc,
        'fn_scc_weaklinks' : do_fn_scc_weaklinks
        }

    func = funcs.get(command)
    if func == None:
        print("No function known called %s. Commands are: %s"%
              (command, " ".join(sorted(funcs.keys()))))
        sys.exit(1)

    cg = make_callgraph(["./src/or/tor"], "./src/", "./src/")
    check_rules(cg)

    func(cg)
